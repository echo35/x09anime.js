const util = require("util");
const path = require("path");
const express = require("express");
const handlebars = require("express-handlebars");
const nineanime = require(path.join(__dirname + "/nineanime.js"));

var app = express();
app.engine("handlebars", handlebars({
    extname: ".handlebars", 
    defaultLayout: "frame", 
    layoutsDir: path.join(__dirname + "/views/layouts/")
}));
app.set("view engine", "handlebars");
app.set("PORT", process.env.PORT || 3000);
app.use(express.json());
app.use("/", express.static(path.join(__dirname + "/static/")));
express.static.mime.define({ 'text/plain': ['handlebars'] });

var appinfo = {
    name: "NodeAnime",
    version: "v1.0.0"
}

app.use(function(req, res, next) {
    console.log(util.format("%s %s %s", (req.headers["X-Forwarded-For"] || req.hostname), req.method, req.path));
    next();
});

app.get("/", function(req, res) {
    res.render("search", { 
        appinfo: appinfo 
    });
});

app.get("/search/:keyword", function(req, res) {
    nineanime.searchSeries(req.params.keyword, data => {
        res.json(data);
    });    
});

app.get("/list/:url", function(req, res) {
    url = Buffer.from(req.params.url, "base64").toString("ascii");
    nineanime.enumerateEpisodes(url, data => {
        data["appinfo"] = appinfo;
        res.render("list", data);
    });
});

app.get("/get/:url", function(req, res) {
    url = Buffer.from(req.params.url, "base64").toString("ascii");
    nineanime.getDirectLink(url, data => {
        data["appinfo"] = appinfo;
        res.json(data);
    })
});

var service = app.listen({
    host: "0.0.0.0", 
    port: app.get("PORT")
}, function() {
    console.log(util.format("Running on http://%s:%d", "0.0.0.0", app.get("PORT")));
});