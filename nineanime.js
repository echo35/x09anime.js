const util = require("util");
const request = require("request");
const cheerio = require("cheerio");
const la = "0a9de5a4";

function getVars(url, callback) { // tsなどの変数を取得する
    request({
        method: "GET",
        url: url
    }, function(err, res) {
        if (err) return console.error(err.message);
        var $ = cheerio.load(res.body);
        var server = "33";
        var ts = $("html").attr("data-ts");
        callback({
            ts: ts,
            server: server
        });
    });
}

exports.getDirectLink = function(url, callback) {
    function genToken(o, la) {
        function sum(string) {
            var a = 0;
            for (i = 0; i < string.length; i++) {
                a += string.charCodeAt(i) + i;
            }
            return a;
        }
        function secret(t, i) {
        var n, e = 0;
        for (n = 0; n < Math.max(t.length, i.length); n++) {
            e *= (n < i.length) ? i.charCodeAt(n) : 1,
            e *= (n < t.length) ? t.charCodeAt(n) : 1
        }
        return Number(e).toString(16);
        }
        var r = sum(la);
        for (const [key, value] of Object.entries(o)) {
            r += sum(secret(la + key, o[key]));
        };
    // console.log("_=" + r);
        return r
    }

    function convertRapidVideo(url, callback, cookie=null) {
        if (cookie) {
            request({
                method: "POST",
                url: url,
                headers: {
                    "Cookie" : cookie,
                    "Referer" : url,
                    "Content-Type" : "application/x-www-form-urlencoded"
                },
                body: "confirm.x=70&confirm.y=89&block=1"
            }, function(err, res) {
                var $ = cheerio.load(res.body);
                callback({
                    "1080p": $("#videojs > source:last-child").attr("src")
                });
            });
        }
        else {
            request({
                method: "GET",
                url: url
            }, function(err, res) {
                if (err) return console.error(err.message);
                cookie = res.headers["set-cookie"][0].split(";")[0] + "; q=1080; " + res.headers["set-cookie"][1].split(";")[0];
                convertRapidVideo(url, callback, cookie=cookie);
            });
        }
    }

    // data { ts, server, epid }
    getVars(url, data => {
        var epid = url.split("/")[url.split("/").length - 1]
        var token = genToken({
            id: epid,
            ts: data["ts"],
            server: data["server"]
        }, la);
        url = util.format("https://www4.9anime.is/ajax/episode/info?ts=%s&_=%s&id=%s&server=%s", data["ts"], token, epid, data["server"])
        request({
            method: "GET",
            url: url
        }, function(err, res) {
            if (err) return console.error(err.message);
            var url = JSON.parse(res.body)["target"] + "&q=1080p";
            convertRapidVideo(url, callback);
        });
    });
}

exports.enumerateEpisodes = function(url, callback) {

    function magic(i) {
        var n, e = 0;
        for (n = 0; (n < i.length); n++)
            e += i.charCodeAt(n) + n;
        return e
    }

    // url = "https://www8.9anime.is/watch/my-hero-academia-2.6z94"
     // https://www8.9anime.is/ajax/film/servers/6z94
    url_split = url.split(".")
    seriesid = url_split[url_split.length-1]


    request({
        method: "GET",
        url: url
    }, function(err, res) {
        if (err) return console.error(err.message);
        var $ = cheerio.load(res.body);
        var ts = $("html").attr("data-ts");
        url = util.format("https://www8.9anime.is/ajax/film/servers/%s?ts=%s&_=%s", seriesid, ts, underscore);
        var underscore = magic(la) + 48
        console.log(url);
        request({
            method: "GET",
            url: url
        }, function(err, res) {
            if (err) return console.error(err.message);
            var $ = cheerio.load(JSON.parse(res.body)["html"]);
            var results = []
            var title = $("h2.title").text();
            $("div.server[data-name='33'] a").each(function(index, element) {
                var link = "https://www4.9anime.is" + $(element).attr("href");
                results.push({
                    index: index + 1,
                    link: link
                });
            });

            callback({
                title: title,
                results: results
            });

        });
    });
}

exports.searchSeries = function(keyword, callback) {
    request({
        method: "GET",
        url: "https://www4.9anime.is/search?keyword=" + encodeURIComponent(keyword)
    }, function(err, res) {
        if (err) return console.error(err.message);
        var $ = cheerio.load(res.body);
        var posters = $("div.item > div.inner > a.poster > img");
        var data = []
        posters.each(key => {
            data.push({
                title: posters[key].attribs["alt"],
                poster: posters[key].attribs["src"],
                link: posters[key].parent.attribs["href"],
                list: Buffer.from(posters[key].parent.attribs["href"]).toString("base64")
            });
        });

        callback(data);
    });
}
